#!/bin/bash

pid_file="/tmp/timew-pause-on-lock.pid"
notification_icon_path="/usr/share/icons/Adwaita/symbolic/categories/emoji-recent-symbolic.svg"
notification_heading="Timew tracking still running"

export DBUS_SESSION_BUS_ADDRESS="${DBUS_SESSION_BUS_ADDRESS:-unix:path=/run/user/${UID}/bus}"

pid_current=$$

pidcheck () {
    pid=$(pgrep -F $pid_file 2> /dev/null || true)
    if [[ $pid != "" ]]; then
        echo -e "ERROR: Script is already running as ${pid} \nExiting ..."
        exit 1
    else
        echo $pid_current > $pid_file
    fi
}

monitor () {
    gdbus monitor -y -d org.freedesktop.login1 | grep --line-buffered -i "LockedHint" | sed -uE 's/.*LockedHint.*<(.*)>.*/\1/g' |
    while read -r stream ; do
        if [[ $stream = "true" ]]; then
            timew_controll stop
        elif [[ $stream = "false" ]]; then
            timew_controll start
        fi
    done
}

timew_controll() {
    if [[ $(timew) != "There is no active time tracking." ]] && [[ $1 = "stop" ]]; then
        get_task_name
        timew stop
        is_stopped=true
    elif [[ $is_stopped = true ]] && [[ $1 = "start" ]]; then
        timew continue
        notification_body="Timew is still tracking ${task_current}"
        notify
        is_stopped=false
    fi
}

get_task_name() {
    task_current="$(timew | sed -E 's/(Tracking)((\s.+\s?)+)/\2/' | sed -E 's/\s//' | sed -E 's/(\s)+Started.+//' | sed -E 's/(\s)+Current.+//' | sed -E 's/(\s)+Total.+//' | sed -z 's/\n//g')"
}

notify() {
    notify-send -i $notification_icon_path "${notification_heading}" "${notification_body}"
}

pidcheck
monitor
