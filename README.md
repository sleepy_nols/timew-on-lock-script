# timew-pause-on-lock
A script that pauses your running timew tasks on screen lock, so you nerver have to edit runaway times agian.

- Stops timew when screen is locked
- Continues timew when screen is unlocked
- Displays notification on screen unlock, that timew is still running
- Stops timew on system shutdown but not reboot

## Configuration

### Dependencies

- timewarrior
- libnotify

### Setup a systemd user service

Customize `User=` in the .service file.
`User=youruser`

Create a symlink for the systemd units:

`sudo ln -s ~/.scripts/timew-pause/timew-pause-on-lock.service /etc/systemd/system/timew-pause-on-lock.service`\
`sudo ln -s ~/.scripts/timew-pause/timew-pause-on-shutdown.service /etc/systemd/system/timew-pause-on-shutdown.service`

Start and enable the service:

```
sudo systemctl start timew-pause-on-lock
sudo systemctl enable timew-pause-on-lock
```

The shutdown services just needs to be enabled as it will start itself on shutdown.

```
sudo systemctl enable timew-pause-on-shutdown
```
